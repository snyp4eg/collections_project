package collection;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class CharCounterJUnitTest {
    protected static CharCounter charCounter;
    protected static Map<Character, Integer> etalon;
    protected static Cache cache;
    protected static Validator validator;
    protected static String sourceString;

    @BeforeAll
    public static void init() {
        cache = mock(InMemoryCache.class);
        validator = mock(CharCounterValidator.class);
        charCounter = new CharCounter(cache, validator);
        sourceString = "hello world! world?";
        etalon = new HashMap<>();
        etalon.put('h', 1);
        etalon.put('e', 1);
        etalon.put('l', 4);
        etalon.put('o', 3);
        etalon.put(' ', 2);
        etalon.put('w', 2);
        etalon.put('r', 2);
        etalon.put('d', 2);
        etalon.put('!', 1);
        etalon.put('?', 1);
    }

    @AfterAll
    public static void tearDown() {
        sourceString = null;
        charCounter = null;
        etalon = null;
        cache = null;
    }

    @Test
    void shouldReturnCountedCharMapThenCacheIsEmpty() {
        when(charCounter.getCache().isPresent(sourceString)).thenReturn(false);
        assertEquals(charCounter.getCharMap(sourceString), etalon);
        verify(charCounter.getCache(), never()).getCache(sourceString);
        Mockito.reset(cache);
    }
    
    @Test
    void shouldReturnCachedCharMapThenCacheIsNotEmpty() {
        when(charCounter.getCache().isPresent(sourceString)).thenReturn(true);
        when(charCounter.getCache().getCache(sourceString)).thenReturn(etalon);
        assertEquals(charCounter.getCharMap(sourceString), etalon);
        verify(charCounter.getCache(), atLeastOnce()).getCache(sourceString);
        Mockito.reset(cache);
    }
}
