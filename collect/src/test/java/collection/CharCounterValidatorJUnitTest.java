package collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class CharCounterValidatorJUnitTest {
    protected static Validator validator;

    @BeforeAll
    public static void init() {
        validator = new CharCounterValidator();
    }

    @AfterAll
    public static void tearDown() {
        validator = null;
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenNullPassed() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            validator.validate(null);
        });
        assertEquals("Data is NULL!", exception.getMessage());
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenEmptyStringPassed() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
            validator.validate("");
        });
        assertEquals("Data is Empty!", exception.getMessage());
    }
}
