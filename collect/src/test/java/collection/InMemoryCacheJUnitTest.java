package collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class InMemoryCacheJUnitTest {
    protected static Cache cacheSpy;
    protected static Map<String, Map<Character, Integer>> cacheMap;

    @BeforeAll
    public static void init() {
        cacheMap = spy(new HashMap<>());
        cacheSpy = spy(new InMemoryCache(cacheMap));
    }

    @AfterAll
    public static void tearDown() {
        cacheMap = null;
        cacheSpy = null;
    }

    @Test
    void shouldReturnTrueThenCacheIsNotEmpty() {
        String key = "key";
        when(cacheMap.containsKey(key)).thenReturn(true);
        assertEquals(cacheSpy.isPresent(key), true);
        verify(cacheSpy, atLeastOnce()).isPresent(key);
    }

    @Test
    void shouldReturnFalseThenCacheIsEmpty() {
        String key = "key";
        when(cacheMap.containsKey(key)).thenReturn(false);
        assertEquals(cacheSpy.isPresent(key), false);
        verify(cacheSpy, atLeastOnce()).isPresent(key);
    }
}
