package collection;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class CharCounterFormatterJUnitTest {
    protected static CharCounterFofmatter formatterSpy;
    protected static Map<Character, Integer> charMap;
    protected static String sourceString;
    protected static String etalon;

    @BeforeAll
    public static void init() {
        formatterSpy = spy(new CharCounterFofmatter());
        sourceString = "hello world! world?";
        etalon = "hello world! world?" + "\n" + "\"h\" - 1" + "\n" + "\"e\" - 1" + "\n" + "\"l\" - 4" + "\n"
                + "\"o\" - 3" + "\n" + "\" \" - 2" + "\n" + "\"w\" - 2" + "\n" + "\"r\" - 2" + "\n" + "\"d\" - 2" + "\n"
                + "\"!\" - 1" + "\n" + "\"?\" - 1" + "\n";
        charMap = new HashMap<>();
        charMap.put('h', 1);
        charMap.put('e', 1);
        charMap.put('l', 4);
        charMap.put('o', 3);
        charMap.put(' ', 2);
        charMap.put('w', 2);
        charMap.put('r', 2);
        charMap.put('d', 2);
        charMap.put('!', 1);
        charMap.put('?', 1);
    }

    @AfterAll
    public static void tearDown() {
        sourceString = null;
        etalon = null;
        charMap = null;
    }

    @Test
    void shouldReturnEtalonStringThenSourceCharMapPassed() {
        assertEquals(formatterSpy.getFormatedString(sourceString, charMap), etalon);
        verify(formatterSpy, atLeastOnce()).getFormatedString(sourceString, charMap);
    }
}
