package collection;

import java.util.Map;

public interface Cache {

    void putCache(String key, Map<Character, Integer> data);

    Map<Character, Integer> getCache(String key);

    boolean isPresent(String key);
}
