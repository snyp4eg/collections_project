package collection;

public class CharCounterValidator implements Validator {

    @Override
    public void validate(String data) {
        CheckForNullValue(data);
        CheckForEmptyValue(data);
    }

    private void CheckForNullValue(String data) {
        if (data == null)
            throw new IllegalArgumentException("Data is NULL!");
    }

    private void CheckForEmptyValue(String data) {
        if (data.isEmpty())
            throw new IllegalArgumentException("Data is Empty!");
    }

}
