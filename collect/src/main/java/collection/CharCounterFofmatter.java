package collection;

import java.util.Map;

public class CharCounterFofmatter {
    public String getFormatedString(String sourceString, Map<Character, Integer> charMap) {
        StringBuilder strBuild = new StringBuilder(sourceString + "\n");
        char[] charTemplate = replaceDuplicatesWithTemplate(sourceString).toCharArray();
        for (Character symbol : charTemplate) {
            strBuild.append("\"" + symbol + "\" - " + charMap.get(symbol) + "\n");
        }
        return strBuild.toString();
    }

    private static String replaceDuplicatesWithTemplate(String inputString) {
        int position = 1;
        char[] characters = inputString.toCharArray();
        for (int i = 1; i < inputString.length(); i++) {
            int j;
            for (j = 0; j < position; ++j) {
                if (characters[i] == characters[j]) {
                    break;
                }
            }

            if (j == position) {
                characters[position] = characters[i];
                ++position;
            } else {
                characters[position] = 0;
                ++position;
            }
        }
        return getStringWithoutDuplicates(characters);
    }

    private static String getStringWithoutDuplicates(char[] inputChars) {
        StringBuilder stringBuilder = new StringBuilder(inputChars.length);
        for (char character : inputChars) {
            if (character != 0) {
                stringBuilder.append(character);
            }
        }
        return stringBuilder.toString();
    }
}
