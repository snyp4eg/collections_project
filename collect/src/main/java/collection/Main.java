package collection;

public class Main {

    public static void main(String[] args) {
        String sourseString = "hello world! world?";
        CharCounterFacade facade = new CharCounterFacade();
        String result = facade.getCharsCount(sourseString);
        System.out.println(result);
    }

}
