package collection;

import java.util.HashMap;
import java.util.Map;

public class CharCounter {
    private Cache cache;
    private Validator validator;

    public CharCounter(Cache cache, Validator validator) {
        this.cache = cache;
        this.validator = validator;
    }

    public Map<Character, Integer> getCharMap(String sourceString) {
        validator.validate(sourceString);
        Map<Character, Integer> map;
        if (cache.isPresent(sourceString)) {
            map = cache.getCache(sourceString);
        } else {
            map = getSimpleMap(sourceString.toCharArray());
            cache.putCache(sourceString, map);
        }
        return map;
    }

    private static Map<Character, Integer> getSimpleMap(char[] cymbols) {
        Map<Character, Integer> simpleMap = new HashMap<>();
        for (Character symbol : cymbols) {
            if (!simpleMap.containsKey(symbol)) {
                simpleMap.put(symbol, 1);
            } else {
                simpleMap.computeIfPresent(symbol, (k, v) -> ++v);
            }
        }
        return simpleMap;
    }

    public Cache getCache() {
        return cache;
    }
}
