package collection;

public interface Validator {
    public void validate(String sourceString);
}
