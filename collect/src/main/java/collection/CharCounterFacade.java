package collection;

import java.util.HashMap;
import java.util.Map;

public class CharCounterFacade {
    private CharCounter data;

    public CharCounterFacade() {
        Cache cache = new InMemoryCache(new HashMap<>());
        Validator validator = new CharCounterValidator();
        data = new CharCounter(cache, validator);
    }

    public String getCharsCount(String sourceString) {
        Map<Character, Integer> charMap = data.getCharMap(sourceString);
        CharCounterFofmatter formater = new CharCounterFofmatter();
        return formater.getFormatedString(sourceString, charMap);
    }
}
