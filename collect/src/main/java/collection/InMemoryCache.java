package collection;

import java.util.Map;

public class InMemoryCache implements Cache {
    private Map<String, Map<Character, Integer>> cache;

    public InMemoryCache(Map<String, Map<Character, Integer>> cache) {
        this.cache = cache;
    }

    public boolean isPresent(String key) {
        return cache.containsKey(key);
    }

    public void putCache(String key, Map<Character, Integer> data) {
        if (!isPresent(key)) {
            cache.put(key, data);
        }
    }

    public Map<Character, Integer> getCache(String key) {
        return cache.get(key);
    }
}
